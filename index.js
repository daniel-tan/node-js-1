const fs = require("fs");

const listGames = () => {
	let data = fs.readFileSync("games.json", "utf8");
	return JSON.parse(data);
};

const inputGame = (input) => {
	const getGames = listGames();
	getGames.push(input);
	fs.writeFileSync("games.json", JSON.stringify(getGames));
	console.log(listGames());
};

const deleteGame = (id) => {
	let list = listGames();
	const gameFiltered = list.filter((games) => {
		return games.id != id;
	});
	fs.writeFileSync("games.json", JSON.stringify(gameFiltered));
	console.log(listGames());
}

const updateGame = (id, editName) => {
	let lists = listGames();

	lists.every(list => {
		if (list.id == id) {
			console.log("jalan");
			list.name = editName;
			return false;
		}
	});

	fs.writeFileSync("games.json", JSON.stringify(lists));
	console.log(listGames());
}

const detailGame = (id) => {
	let lists = listGames();
	lists.every(list => {
		if (list.id == id) {
			console.log("id = " + list.id + " nama = " + list.name + " genre = " + list.genre);
			return false;
		}
	});
}

if (Number(process.argv[2]) == 1) {
	console.log(listGames());
}

if (Number(process.argv[2]) == 2) {
	detailGame(process.argv[3]);
}

if (Number(process.argv[2]) == 3) {
	updateGame(process.argv[3], process.argv[4]);
}

if (Number(process.argv[2]) == 4) {
	deleteGame(process.argv[3]);
}

if (Number(process.argv[2]) == 5) {
	const cek = listGames();
	const game = {
		id: cek[cek.length - 1].id + 1,
		name: process.argv[3],
		genre: process.argv[4],
	}
	inputGame(game);
}